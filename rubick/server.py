# -*- coding: utf-8 -*-
__version__ = '0.1'
__author__ = 'Rubick <haonchen@cisco.com>'
import os
os.environ.setdefault('SCRAPY_SETTINGS_MODULE', 'rubick.settings')


from tornado import web, ioloop, options
from cache_helper import CacheHelp
from scrapy import cmdline
from whoosh_research import WhooshResearch

from scrapy.conf import settings
from rubick.spiders.spider_man import MuTongZX
from scrapy.crawler import CrawlerProcess


# def update_resource():
#     name = 'muton'
#     cmd = 'scrapy crawl {0}'.format(name)
#     cmdline.execute(cmd.split())

# def load_resource()


if __name__ == "__main__":
    options.define("p", default=7777, help="run on the given port", type=int)
    options.parse_command_line()
    port = options.options.p

    """
    Init whoosh engine
    """
    whoosh_research = WhooshResearch()
    whoosh_research.load_all()

    """
    Init first scrap
    """
    crawler = CrawlerProcess(settings)
    crawler.crawl(MuTongZX)
    crawler.start()

    print 'Tornado server started on port %d' % port
    print 'Press "Ctrl+C" to exit.\n'
    web.Application([
        # (r'/api/v1/devicestatus', StatusCheckApiHandler)
    ], autoreload=True).listen(port)
    try:
        ioloop.IOLoop.instance().start()
    except KeyboardInterrupt:
        pass