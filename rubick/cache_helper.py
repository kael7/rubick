import threading
import time
import os
from constant import PathConstant
global_lock = threading.Lock()


def local_resource(func):
    def wrapper(*args, **kwargs):
        global_lock.acquire()
        res = func(*args, **kwargs)
        global_lock.release()
        return res
    return wrapper


class SingTone(object):
    _instance = None

    def __new__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = super(SingTone, cls).__new__(cls, *args, **kwargs)
        return cls._instance


class CacheHelp(SingTone):
    def __init__(self):
        super(CacheHelp, self).__init__()
        if hasattr(self, 'current_cached') is False:
            self.current_cached = []

    def _set_key(self, _date):
        self.current_cached.append(_date)

    @local_resource
    def update(self):
        for _file in os.listdir(PathConstant.RESOURCE_PATH):
            _key = _file[0:10]
            if _key not in self.current_cached:
                self._set_key(_key)

    @local_resource
    def is_exist(self, _key):
        return _key in self.current_cached




