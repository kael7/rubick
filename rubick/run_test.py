import os
os.environ.setdefault('SCRAPY_SETTINGS_MODULE', 'rubick.settings')
from scrapy.conf import settings
from rubick.spiders.spider_man import MuTongZX
from scrapy.crawler import CrawlerProcess
crawler = CrawlerProcess(settings)
crawler.crawl(MuTongZX)
crawler.start()
