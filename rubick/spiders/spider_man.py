#coding=utf-8
import scrapy
import re
from rubick.items import RubickItem
from rubick.cache_helper import CacheHelp, SingTone


class MuTongZX(SingTone, scrapy.Spider):
    name = 'mutong'
    allowed_domains = ['mutongzixun.com']
    base_pages = 'http://www.mutongzixun.com/index.php?m=content&c=index&a=lists&catid=8&page=%s'
    base_domain = "http://www.mutongzixun.com%s"
    summary_url_set = set()
    content_url_set = set()

    def start_requests(self):
        urls = ['http://www.mutongzixun.com/index.php?m=content&c=index&a=lists&catid=8']
        for url in urls:
            yield scrapy.Request(url, callback=self.parse)

    def parse(self, response):
        item = RubickItem()
        pages_link = []
        pages = self._parser_pages_links(response)
        oldest_page = pages[-2]
        latest_page = pages[2]
        pages_link = []
        for peer in range(int(latest_page), int(oldest_page)):
            page_link = self.base_pages % peer
            if page_link not in self.summary_url_set:
                MuTongZX.summary_url_set.add(page_link)
                yield scrapy.Request(page_link, callback=self._parser_summary_links)

    @staticmethod
    def _parser_pages_links(response):
        pages_link = response.xpath('//div[@id="pages"]/a/text()').extract()
        return pages_link

    def _parser_summary_links(self, response):
        contents_link = response.xpath('//div[@class="content"]//a//@href').extract()
        for content_link in contents_link:
            content_link = self.base_domain % content_link
            if content_link not in self.content_url_set:
                self.content_url_set.add(content_link)
                yield scrapy.Request(content_link, callback=self._parser_content_links)
        pass

    def _parser_content_links(self, response):
        date_pattern = "(\d+-\d+-\d+)\s+(\d+:\d+:\d+)"
        item = RubickItem()
        item['content'] = (response.text).encode(response.encoding)
        item['url'] = response.url
        item['title'] = response.xpath("//title/text()").extract()[0]
        m = re.search(date_pattern, str(response.xpath("//span[@class='author']/text()").extract()))
        if m:
            date = m.groups()[0]
        else:
            date = "Unknown Date"
        item['date'] = date
        yield item





