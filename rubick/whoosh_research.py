#coding=utf-8
from whoosh.index import create_in
from whoosh.fields import *
from whoosh.qparser import QueryParser
from jieba.analyse import ChineseAnalyzer
from constant import PathConstant, _init_necessary_ele
import os
import zipfile
import uuid
CSS_STYLE = "<style style='text/css'>b{color:black;background-color:red}</style>"


class WhooshResearch(object):
    def __init__(self):
        self.bash_resource_path = PathConstant.RESOURCE_PATH
        self._cache = []
        analyzer = ChineseAnalyzer()
        schema = Schema(title=TEXT(stored=True), path=ID(stored=True), content=TEXT(stored=True, analyzer=analyzer))
        indexdir = os.path.join(os.path.dirname(__file__), "indexdir")
        if os.path.exists(indexdir) is False:
            os.mkdir(indexdir)
        self.idx = create_in("indexdir", schema)
        self.writer = self.idx.writer()
        pass

    def _load_resource(self, filename):
        try:
            if filename not in self._cache:
                _file = os.path.join(self.bash_resource_path, filename)
                with open(_file, 'r') as f:
                    content = f.read()
                self.writer.add_document(
                    title=unicode(filename, 'GB2312'),
                    content=content.decode('utf-8')
                )
        except Exception, e:
            print str(e)

    def load_all(self):
        for _file in os.listdir(self.bash_resource_path):
            self._load_resource(_file)
        self.writer.commit()

    def search_key_world(self, key_word):
        searcher = self.idx.searcher()
        parser = QueryParser("content", schema=self.idx.schema)
        q = parser.parse(key_word)
        results = searcher.search(q, limit=99999, collapse_limit=5)
        results.fragmenter.charlimit = None
        results.fragmenter.maxchars = 99999
        results.fragmenter.surround = 99999
        return_result = []
        for hit in results:
            return_result.append(
                {
                    "title": hit['title'],
                    "content": self._decorate_into_html(hit.highlights("content", top=99999))
                }
            )
        return return_result

    def _decorate_into_html(self, results):
        results = results.replace("&lt;", "<")
        results = results.replace("&gt;", ">")
        results = results.replace("nbsp;", " ")
        results = results.replace("&amp;", "")
        return results

    def zip_result(self, results):
        zip_filename = os.path.join(PathConstant.RESULT_PATH, '%s.zip' % uuid.uuid4())
        azip = zipfile.ZipFile(zip_filename, 'w')
        for result in results:
            filename = result['title']
            content = CSS_STYLE + result['content'].encode('utf-8')
            azip.writestr(filename, bytes=content, compress_type=zipfile.ZIP_DEFLATED)
        azip.close()
        return azip

    def create_result(self, key_world):
        results = self.search_key_world(key_world)
        return self.zip_result(results)


if __name__ == "__main__":
    whoosh_search = WhooshResearch()
    whoosh_search.load_all()
    _init_necessary_ele()
    whoosh_search.create_result(u'' + "淘金".decode("utf-8"))

