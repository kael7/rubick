import os


class PathConstant(object):
    RESOURCE_DIR = 'resource'
    RESOURCE_PATH = os.path.join(os.path.dirname(__file__), RESOURCE_DIR)
    RESULT_DIR = 'result'
    RESULT_PATH = os.path.join(os.path.dirname(__file__), RESULT_DIR)


def _init_necessary_ele():
    """
    Init necessary element for project
    :return:
    """
    target_class = [PathConstant]
    for _class in target_class:
        for _dir in dir(_class):
            if str(_dir).startswith("_") is False and str(_dir).endswith("PATH"):
                _path = getattr(_class, str(_dir))
                if os.path.exists(_path) is False:
                    os.mkdir(_path)


_init_necessary_ele()