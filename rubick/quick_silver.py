#coding=utf-8
import os
import constant


class QuickSilver(object):
    def process_item(self, item, spider):
        constant._init_necessary_ele()
        _file_path = os.path.join(constant.PathConstant.RESOURCE_PATH, item['date'] + item['title'] + ".html")
        with open(_file_path, 'w') as fp:
            fp.write(item['content'])